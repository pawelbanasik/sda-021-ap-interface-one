package com.pawelbanasik;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		Animal cat = new Cat();
		Animal dog = new Dog();

		Animal[] animals = new Animal[] { cat, dog };
		for (Animal animal : animals) {
			animal.makeVoise();
		}

		List<Animal> animalList = new ArrayList<>();
		animalList.add(cat);
		animalList.add(dog);

		for (Animal animal : animalList) {
			animal.makeVoise();
		}

	}
}
